var login = 1;
var libLoc = "";
var barcode_temp = [];
function startTime() 
{
    if( libLoc == "" )
      document.getElementById('txt').innerHTML = "หนังสือ Book Delivery";
    else
      document.getElementById('txt').innerHTML = "หนังสือ Book Delivery"+libLoc;
    var t = setTimeout(function(){startTime()},50);

    for (var i = 1; i < document.getElementById('table').rows.length; i++) { 
      var rowDel = document.getElementById("table").rows[i].cells;
      rowDel[0].innerHTML=i;
    }


}

function checkTime(i) {
    if (i<10) {i = "0" + i};
    return i;
}



function prt(divName) 
{
    var today=new Date();
    var h=today.getHours();
    var mi=today.getMinutes();
    var s=today.getSeconds();
    var y=today.getFullYear()+543;
    var m=today.getMonth();
    var d=today.getDate();
    var w=today.getDay();
    h = checkTime(h);
    mi = checkTime(mi);
    s = checkTime(s);
    m = checkTime(m);
    d = checkTime(d);
    var week;

    if(w==0){
      week = "อาทิตย์";
    }else if(w==1){
      week = "จันทร์";
    }else if(w==2){
      week = "อังคาร";                   
    }else if(w==3){
      week = "พุธ";                       
    }else if(w==4){
      week = "พฤหัสบดี";                        
    }else if(w==5){
      week = "ศุกร์";                
    }else if(w==6){
      week = "เสาร์";              
    }
    var originalContents = document.body.innerHTML;
    document.getElementById('txt').innerHTML = "หนังสือ Book Delivery มาถึง"+libLoc+"วัน "+week+" ที่ "+d+"/"+m+"/"+y+"    "+h+":"+mi+":"+s+"น.";
    document.getElementById('txt').style = 'text-align:center';
    var t = setTimeout(function(){startTime()},50);
     
     var count_table = document.getElementsByTagName('tr').length;
       for(var x = 0;x < count_table;x++)
       {
           var row = document.getElementById('table').rows[x];                              
            row.deleteCell(-1);
            var name_rec = row.insertCell(-1);
            if( x == 0 )
              name_rec.innerHTML="ชื่อผู้รับหนังสือ/วันที่";
            else
              name_rec.innerHTML="";
            name_rec.style = 'width:10%;max-width:10%';
                     
  
            var staff = row.insertCell(-1);
            if( x == 0 )
              staff.innerHTML="เจ้าหน้าที่";
            else
              staff.innerHTML="";
            staff.style = 'width:10%;max-width:10%';
       }
     var thead = document.getElementsByTagName('thead').length;
       console.log(thead);
         
     var printContents = document.getElementById(divName).innerHTML;   
     
     printContents = printContents.fontsize(2);
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}

function dl_bc()
{

        var textToWrite = "";
        var textToWrite = $('textarea#barcode').val().replace(/\n/g,'\r\n');
        
        var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
        var fileNameToSaveAs = "barcode";

        var downloadLink = document.createElement("a");
        downloadLink.download = fileNameToSaveAs;
        downloadLink.innerHTML = "Download File";
        if (window.webkitURL != null)
        {
          downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
        }
        else
        {
          // Firefox requires the link to be added to the DOM
          // before it can be clicked.
          downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
          //downloadLink.onclick = destroyClickedElement;
          downloadLink.style.display = "none";
          document.body.appendChild(downloadLink);
        }

        downloadLink.click();
}

function exp()
{
   var data = "<html>"+
              "<head>"+
              "<script src = 'https://staff.traindb.hylib.org/intranet-tmpl/test/request.js'></script>"+
              "<script src = 'https://staff.traindb.hylib.org/intranet-tmpl/test/jquery-2.1.1.min.js'></script>"+
              "</head>"+
              "<title>Export Barcode</title>"+
              
              "<textarea id = 'barcode' style='width:500px;height:500px;;margin:10px;paddind:10px'>";
    var b = "";
        for(var x = 0;x < barcode_temp.length;x++) 
        {
          b += barcode_temp[x]+"\n";
        }       
   data += b+"</textarea><br><button onclick = 'dl_bc()'>Download Barcode as text</button>";    
   myWindow = window.open("data:text/html," + encodeURIComponent(data),
                       "_blank");
   //myWindow.arr = barcode_temp;
   myWindow.focus();
}

function ins(id,e) 
{    

    document.getElementById('topheader').style.cursor="progress";
    document.getElementById('table').style.cursor="progress";
    document.getElementById('barcode').style.cursor="progress";
    document.getElementById('sub').style.cursor="progress";
    e.preventDefault();
    var arr = []; 
    var barcode = document.getElementById('barcode');
    barcode_temp.push(barcode.value);
    turn = 'true';
    var link = "/cgi-bin/koha/catalogue/search.pl?idx=bc&q="+barcode.value;
    $.ajax({
        url:link,
        type: "GET",
        data:{},
        dataType:'html',
        timeout: 200000,
        success: function(data)
        {
            
            var el = $( '<div></div>' );
            el.html(data);
            libLoc = $('a.dropdown-toggle strong',el).text();
         
           
            if($('p.submit',el).length > 0)
             login = -1;
            else
             login = 1;
           

			      var sel = ($("a[href*='/cgi-bin/koha/catalogue/moredetail.pl']", el).filter(function()
            {
                return $(this).text() == barcode.value;
            }).parent().parent());

            var el2 = $( '<div></div>' );
            el2.html(sel);
            var location = $("td.location",el2);
            var coll = $("td",el2).eq(3);
            var itemcallnumber = $("td.itemcallnumber",el2);
            var name = $("td.status a",el2);
            var copynumber = $("td.copynumber",el2);
            var title = $("div#breadcrumbs i",el);

            arr.push(name.text());
            arr.push(coll.text());
            arr.push(itemcallnumber.text());
            arr.push(copynumber.text());
            arr.push(barcode.value);
            arr.push(title.text());
            arr.push(location.text());

           
			      if( arr[0] == "" )
				      arr[0] = 'Item not checkout';
            
            var tr = document.createElement("Tr");
            tr.setAttribute("align", "center");
            tr.setAttribute("padding", "5px");
            document.getElementById("body").appendChild(tr);

            var td1 = document.createElement("TD");
            var c1 = document.createTextNode(document.getElementById('table').rows.length-1);
            td1.appendChild(c1);
            tr.appendChild(td1);

            var td2 = document.createElement("TD");
            var c2 = document.createTextNode(arr[0]);
            td2.appendChild(c2);
            tr.appendChild(td2);

            var td3 = document.createElement("TD");
            var c3 = document.createTextNode(arr[1]);
            td3.appendChild(c3);
            tr.appendChild(td3);

            var td4 = document.createElement("TD");
            var c4 = document.createTextNode(arr[2]);
            td4.appendChild(c4);
            tr.appendChild(td4);

            var td5 = document.createElement("TD");
            var c5 = document.createTextNode(arr[3]);
            td5.appendChild(c5);
            tr.appendChild(td5);

            var td6 = document.createElement("TD");
            var c6 = document.createTextNode(arr[4]);
            td6.appendChild(c6);
            tr.appendChild(td6);

            var td7 = document.createElement("TD");
            var c7 = document.createTextNode(arr[5]);
            td7.appendChild(c7);
            tr.appendChild(td7);

            var td8 = document.createElement("TD");
            var c8 = document.createTextNode(arr[6]);
            td8.appendChild(c8);
            tr.appendChild(td8);

            var td11 = document.createElement("TD");
            var c11 = document.createTextNode("");
            td11.appendChild(c11);
            tr.appendChild(td11);

          	var button = document.createElement("BUTTON");
          	button.setAttribute("onclick", "del(this)");
          	var ch1 = document.createTextNode("Delete");
          	button.appendChild(ch1);
          	td11.appendChild(button);
            
        		checkErr(); 


            document.getElementById('topheader').style.cursor="default";
            document.getElementById('table').style.cursor="default";
            document.getElementById('barcode').style.cursor="default";
            document.getElementById('sub').style.cursor="default";
          
    	},
    	error:function(error){alert("error"+JSON.stringify(error));}
  	});
}

function del(row){
    var numRow = row.parentNode.parentNode.rowIndex;
    document.getElementById('table').deleteRow(numRow);

    for (var i = numRow; i < document.getElementById('table').rows.length; i++) { 
      var rowDel = document.getElementById("table").rows[i].cells;
      rowDel[0].innerHTML=i;
    }
  
}